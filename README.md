# dooley/cowsay - a sinatra/docker example
- an example of a sinatra application running in Docker

The internet does not seem to want to show us a simple yet complete example of a containerized ruby web application.  Here is one.

The idea is not my own, but the code is.

My plan, at the moment, is to create various git branches with additional features, servers, and techniques.  ATM this contains some basic stuff like [spawn](./src/server.rb#22) and [string concatenation](https://www.rubyguides.com/2019/07/ruby-string-concatenation/)

# BUILD
```
docker build . -t dooley/cowsay
```
**NOTE**: My local build environment includes a [gem server](https://guides.rubygems.org/run-your-own-gem-server/).  Earthlings must change Gemfile, line #1 from
```
source "http://172.17.0.1:8081/repository/ruby/"
```
to
```
source 'https://rubygems.org'
```
before building.
# RUN (with imported volume for development work)
```
docker run -dt -p 4567:4567 -v `pwd`/src:/app/src dooley/cowsay
```
Now you can make changes to the files in [src](./src) and have those changes detected by the running container. (Some work yet to do so that the server can be restarted without affecting the container)

# VALIDATE
Navigate to [cowsay](http://localhost:4567/cowsay?message=Yay\!+Ruby+is+so+cool\!)

# Screenshot
![Ghost][ghost] 

[ghost]: ./public/ghost_buster.png "Screen Shot"