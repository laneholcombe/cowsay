FROM ubuntu:18.04
LABEL maintainer="lane@joeandlane.com"

ENV BUNDLE_SILENCE_ROOT_WARNING true
RUN apt-get -yq update && apt-get -yq install ruby-full nginx build-essential cowsay
WORKDIR /app
ADD . /app
RUN gem install bundler
RUN bundler install

#EXPOSE 8808 80 4567

ENTRYPOINT [ "ruby", "./src/server.rb", "-o", "0.0.0.0" ]
