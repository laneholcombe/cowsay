# Requires the Gemfile
require 'bundler' 
require 'json'
require 'open3'

Bundler.require

# On start let's get a list of available cows.
# In cowsay terms this means get the output of 'cowsay -l'
# into an array

$cows = Array.new

cows_found = false
stdout, stderr, status = Open3.capture3('/usr/games/cowsay -l')
output = stdout.split
output.each { | cow | 
  if cows_found
    $cows << cow
  else
    if cow.end_with? ":"
      cows_found = true;
    end
  end
}

# Let's setup what (s)he looks like

$default_cow = Hash.new
$default_cow["message"] = "meep"
$default_cow["type"] = ""
$default_cow["think"] = true

# Read index.html
$index = File.read(File.join('public', 'index.html'))

def cowsay(cow)
  puts "cowsay(#{cow.inspect})"
  page = $index.dup
  flags = ""
  if cow["eyes"] && cow["eyes"] != ""
    flags = "-e '" + cow["eyes"][0,2] + "' "
    page.sub! "%%eyes%%", cow["eyes"][0,2]
  else
    page.sub! "%%eyes%%", ""
  end
  if cow["tongue"] && cow["tongue"] != ""
    flags = flags + "-T '" + cow["tongue"][0,2] + "' "
    page.sub! "%%tongue%%", cow["tongue"][0, 2]
  else
    page.sub! "%%tongue%%", ""
  end
  command = "/usr/games/cowsay"

  if cow["think"] == true
    command = "/usr/games/cowthink"
    page.sub! "%%think%%", "checked"
  else
    page.sub! "%%think%%", ""
  end
  type = ""
  cow_string = "<option value='cow' selected>cow</option>"
  if cow["type"] && cow["type"] != ""
    type = "-f " + cow["type"]
    cow_string = "<option value='cow'>cow</option>"
  else
    puts "Selected cow is default"
  end
  $cows.each { | c |
    if c != cow["type"]
      cow_string = cow_string + "<option value='" + c + "'>" + c + "</option>"
    else
      puts "Selected cow is " + c
      cow_string = cow_string + "<option value='" + c + "' selected>" + c + "</option>"
    end
  }
  page.sub! "%%COWS%%", cow_string

  "borg dead greedy paranoid stoned tired wired young".split.each { | key |
    puts "DEBUG: " + key + " = " + page[key]
    if cow[page[key]]
      puts "page[#{key}] is #{page[key]}"
      flags = flags + "-" + page[key][0,1] + " "
      page.sub! "%%" + key + "%%", "checked"
    else
      page.sub! "%%" + key + "%%", ""
    end
  }
  message = "'" + cow["message"] + "'"

  page.sub! "%%message%%", cow["message"]
  page.sub! "%%cowsay%%", cow["message"]
  page.sub! "%%eyes%%", cow["eyes"] ? cow["eyes"] : ""
  page.sub! "%%tongue%%", cow["tongue"] ? cow["tongue"] : ""
  puts "Executing command [#{command} #{type} #{flags} "#{message}""]"
  stdout, stderr, status = Open3.capture3("#{command} #{type} #{flags} #{message}")
  puts stdout
  page.sub! "%%YOURCOW%%", stdout
  puts "Returning " + page
  page
end

# Provide form input
get '/' do
  cowsay($default_cow)
end

# Serve static
get '*' do
  file_path = File.join('public', request.path)
  File.exist?(file_path) ? send_file(file_path) : halt
end

# Process form input
post '/' do
  cow = Hash.new

  request.POST.each { |k,v| 
    case k
    when "message"
      cow["message"] = v
    when "type"
      if v != "cow"
        cow["type"] = v
      end
    when "eyes"
      cow["eyes"] = v
    when "tongue"
      cow["tongue"] = v
    when "think"
      cow["think"] = true
    else
      cow[k] = true
    end
  }
  cowsay cow 
end

